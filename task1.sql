DROP TABLE IF EXISTS docs CASCADE;

CREATE TABLE IF NOT EXISTS docs (
    doc_cod SERIAL NOT NULL,
    doc_num VARCHAR(10) NOT NULL,
    in_out_flag INT NOT NULL,
    state INT NOT NULL DEFAULT 0,
    modified_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (doc_cod)
);

DROP TABLE IF EXISTS goods CASCADE;

CREATE TABLE IF NOT EXISTS goods (
    good_cod INT NOT NULL,
    doc_cod INT NOT NULL,
    qty INT NOT NULL,
    price INT NOT NULL,
    CONSTRAINT fk_goods_docs FOREIGN KEY (doc_cod) REFERENCES docs (doc_cod) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO docs (doc_num, in_out_flag, state)
VALUES ('00001', 0, 1),
    ('00002', 0, 1),
    ('00003', 0, 1),
    ('00004', 0, 1),
    ('00005', 0, 1),
    ('00006', 1, 1),
    ('00007', 1, 1),
    ('00008', 1, 1),
    ('00009', 1, 1),
    ('00010', 1, 1);

INSERT INTO goods (good_cod, doc_cod, qty, price)
VALUES (1, 1, 100, 10000),
    (2, 1, 50, 20000),
    (3, 2, 50, 30000),
    (4, 2, 60, 40000),
    (1, 2, 50, 10000),
    (2, 3, 70, 20000),
    (3, 3, 80, 30000),
    (5, 4, 15, 50000),
    (5, 5, 50, 50000),
    (1, 6, 50, 11000),
    (2, 6, 40, 22000),
    (3, 7, 45, 33000),
    (4, 7, 60, 44000),
    (1, 7, 49, 11000),
    (2, 8, 70, 22000),
    (3, 8, 10, 33000),
    (5, 9, 14, 55000),
    (5, 10, 5, 55000);

SELECT goods.good_cod,
    SUM(
        CASE
            WHEN in_out_flag = 1 THEN qty * price
            ELSE qty * price * -1
        END
    ) AS balance
FROM goods
    JOIN docs ON docs.doc_cod = goods.doc_cod
GROUP BY good_cod
ORDER BY good_cod ASC