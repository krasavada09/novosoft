DROP FUNCTION IF EXISTS min3(numeric, numeric, numeric);

CREATE FUNCTION min3(
    pVal1 numeric DEFAULT null,
    pVal2 numeric DEFAULT null,
    pVal3 numeric DEFAULT null
) RETURNS numeric AS $$
SELECT LEAST(pVal1, pVal2, pVal3);
$$ LANGUAGE SQL;

SELECT min3(5.0, 4.2, -1.5);