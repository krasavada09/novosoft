DROP TABLE IF EXISTS lpu CASCADE;

CREATE TABLE IF NOT EXISTS lpu (
    id SERIAL NOT NULL,
    name VARCHAR(99) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS cablabs CASCADE;

CREATE TABLE IF NOT EXISTS cablabs (
    id SERIAL NOT NULL,
    name VARCHAR(99) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS employers CASCADE;

CREATE TABLE IF NOT EXISTS employers(
    id SERIAL NOT NULL,
    firstname VARCHAR(30) NOT NULL,
    lastname VARCHAR(30) NOT NULL,
    middlename VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS visits CASCADE;

CREATE TABLE IF NOT EXISTS visits (
    id SERIAL NOT NULL,
    employer_id INT NOT NULL,
    visit_date DATE NOT NULL DEFAULT CURRENT_DATE,
    lpu_id INT NOT NULL,
    cablab_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_visits_lpu1 FOREIGN KEY (lpu_id) REFERENCES lpu (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_visits_cablabs1 FOREIGN KEY (cablab_id) REFERENCES cablabs (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_visits_employers1 FOREIGN KEY (employer_id) REFERENCES employers (id) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS divisions CASCADE;

CREATE TABLE IF NOT EXISTS divisions (
    id SERIAL NOT NULL,
    code VARCHAR(30) NOT NULL,
    name VARCHAR(99) NOT NULL,
    lpu_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_divisions_lpu1 FOREIGN KEY (lpu_id) REFERENCES lpu (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO lpu (name)
VALUES ('ЛПУ 1'),
    ('ЛПУ 2'),
    ('ЛПУ 3');

INSERT INTO cablabs (name)
VALUES ('Кабинет 11'),
    ('Кабинет 12'),
    ('Кабинет 13'),
    ('Кабинет 21'),
    ('Кабинет 22'),
    ('Кабинет 23'),
    ('Кабинет 31'),
    ('Кабинет 32'),
    ('Кабинет 33');

INSERT INTO employers (firstname, lastname, middlename)
VALUES ('Vernon', 'Garner', 'G.'),
    ('Moana', 'Galloway', 'K.'),
    ('Stuart', 'Wilcox', 'I.'),
    ('Dustin', 'Oliver', 'Z.'),
    ('Kristen', 'Ferrell', 'E.'),
    ('Sybill', 'Caldwell', 'T.'),
    ('Gage', 'Sampson', 'Y.'),
    ('Cherokee', 'Moreno', 'O.'),
    ('Harlan', 'Hardy', 'X.'),
    ('Jonah', 'Woodard', 'G.'),
    ('Ronan', 'Knapp', 'S.'),
    ('Yardley', 'Nichols', 'O.'),
    ('Avye', 'Rojas', 'P.'),
    ('Heidi', 'Spears', 'X.');

INSERT INTO visits (employer_id, visit_date, lpu_id, cablab_id)
VALUES (1, '2022-09-01', 1, 1),
    (2, '2022-09-01', 1, 1),
    (3, '2022-09-01', 1, 1),
    (1, '2022-09-01', 2, 2),
    (2, '2022-09-02', 2, 2),
    (3, '2022-09-02', 2, 2),
    (1, '2022-09-02', 3, 2),
    (2, '2022-09-02', 3, 1),
    (3, '2022-09-02', 3, 1);

INSERT INTO divisions (code, name, lpu_id)
VALUES ('1', 'Подразделение 1', 1),
    ('2', 'Подразделение 2', 1),
    ('3', 'Подразделение 3', 2),
    ('4', 'Подразделение 4', 2),
    ('5', 'Подразделение 5', 3),
    ('6', 'Подразделение 6', 3);

SELECT COUNT(cablabs.name),
    cablabs.name AS cablab,
    visit_date
FROM visits
    JOIN cablabs ON (visits.cablab_id = cablabs.id)
GROUP BY visit_date, cablabs.name
ORDER BY visit_date, cablabs.name